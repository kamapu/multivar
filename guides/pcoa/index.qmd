---
title: Principal Coordinate Analysis
#author: Miguel Alvarez
date: '2024-01-26'
date-modified: '`r Sys.Date()`'
#image: iris-stats.png
categories:
  - method
description:
  Principal Coordinate Analysis (PCOA) is a distance-based ordination that aims to compress similarity patterns into an ordination diagram.
draft: false
---

# Description

Principal Coordinate Analysis (PCOA), also known as classical (metric) multidimensional scaling, is a statistical technique used to visualise and analyse (dis)similarities between a set of objects or observations.
PCOA transforms a distance matrix derived from pairwise dissimilarity measures into a set of orthogonal axes (principal coordinates) in a lower dimensional space.
These axes represent the main directions in the data that account for the maximum variance.

# Example


```{r}
#| echo: false
lalashan <- readRDS("../../data/lalashan.rds")
```

```{r}
#| eval: false
lalashan <- readRDS("lalashan.rds")
```

```{r}
library(vegan)

# First calculate distance matrix
dist_m <- vegdist(lalashan$cross, method = "bray")

# Compute the ordination
pcoa_ord <- wcmdscale(dist_m, eig = TRUE)
plot(pcoa_ord)
```


# Alternative functions

- `vegan::wcmdscale()`
- `vegan::capscale()` if using only one matrix as input
- `stats::cmdscale()`
- `ape::pcoa()`


# Further References

- <https://www.davidzeleny.net/anadat-r/doku.php/en:pcoa_nmds>
- <https://en.wikipedia.org/wiki/Multidimensional_scaling>
