---
title: Canonical Correspondence Analysis
date: '2024-02-02'
date-modified: '`r Sys.Date()`'
#image: iris-stats.png
categories:
  - method
description:
  Canonical Correspondence Analysis (CCA) is a constrained multivariate
  ordination used to explore relationships between species abundance data and
  environmental variables when unimodal responses are expected.
draft: false
---

# Description

Canonical Correspondence Analysis (CCA) is a multivariate statistical method widely used in ecological research to explore the relationships between species abundance data and environmental variables.
CCA is an extension of [CA](../ca/index.qmd) and aims to maximize the covariance between species abundances and environmental gradients while respecting the constraints of linear ordination.

# Example

```{r}
#| echo: false
wetlands <- readRDS("../../data/wetlands.rds")
```

```{r}
#| eval: false
wetlands <- readRDS("wetlands.rds")
```

```{r}
library(vegan)

cca_ord <- cca(wetlands$cross, wetlands$env[ , c(
            "groundwater",
            "tmean",
            "tseason",
						"prsum",
            "prseason")])
plot(cca_ord)
```

<!-- 

# Alternative functions

- `vegan::cca()` when only one table is provided as input.
- `ade4::dudi.coa()`


# Further References

- <https://www.davidzeleny.net/anadat-r/doku.php/en:ca_dca>
- <https://en.wikipedia.org/wiki/Correspondence_analysis>
-->
