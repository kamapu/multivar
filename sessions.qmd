---
title: Sessions
---

# workshop 2 -- Assessing Explanatory Variables

*2^nd^ -- 3^rd^ February 2024*

## Description

Assessing vegetation species composition as a response to environmental factors is not trivial, as most common modelling approaches are restricted to a single response variable and not multiple responses.
Multivariate statistics, including direct ordination analysis (redundancy analysis and canonical correspondence analysis), are tools to address this problem.

In this workshop we will focus on direct estimation of the importance of environmental variables in determining species composition, and will also cover methods such as the PERMANOVA test and the Canonical Test on Principal Coordinates.

For more details see the [workshop programm](description-workshop-2.pdf).

## Materials

- [**Slides**](slides-w2.pdf)
- [Script Session 1](workshop2-session1.R)
- [Script Session 2](workshop2-session2.R)


# Workshop 1 -- Assessing Diversity and Species Composition

*26^th^ -- 27^th^ January 2024*

## Description

Plant species composition as a response to environmental factors and anthropogenic disturbance is a central principle in vegetation science and biogeography.
Poor resource availability and time constraints can limit data collection, sampling and laboratory analysis, so several research projects will rely on this principle and draw conclusions based on species composition alone.

This workshop focusses on indirect (unconstrained) ordination methods, namely, principal component analysis, (detrended) correspondence analysis, principal coordinate analysis and non-metric multidimensional scaling.

For more details see the [workshop programm](description-workshop-1.pdf).

## Materials

- [**Slides**](slides-w1.pdf)
- [Script Session 1](day1-session1.R)
- [Script Session 2](day1-session2.R)
- [Script Session 3](day2-session3.R)
- [Script Session 4](day2-session4.R)
