
library(quarto)
library(zip)

root_path <- "../../courses-miguel/multivariate-ordination"

# Import data
Files <- list.files(file.path(root_path, "data"), pattern = ".rds",
    full.names = TRUE)
file.copy(from = Files, to = "data", overwrite = TRUE)

# Create zip-file
unlink("data/course-data.zip")
zip("data/course-data.zip", Files, mode = "cherry-pick")

# Copy descriptions
file.copy(from = file.path(root_path, "documents",
            c("description-workshop-1.pdf", "description-workshop-2.pdf")),
            to = ".", overwrite = TRUE)

# Copy bibliographic database
file.copy(from = "../../db-dumps/literatur_db/bib/MiguelReferences.bib",
to = ".", overwrite = TRUE)

# Copy slides
file.copy(from = file.path(root_path, c("slides-w1.pdf", "slides-w2.pdf")),
    to = ".", overwrite = TRUE)

# Build the page and preview
system(command = "quarto render . --output-dir=public/")

system(command = "quarto preview")

# Commit changes ----
system(command = paste(c(
            "git add .",
            "git commit -m \"Update from R.\"",
            "git push"),
        collapse = " && "))
